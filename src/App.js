import React, { useEffect, useState } from "react";
import axios from "axios";
import ProjectCard from "./components/ProjectCard";

const API_URL = "https://unreolfreedombackendservice.pythonanywhere.com/list/";

export default function App() {
  const [projectDatas, setProjectDatas] = useState([]);
  useEffect(() => {
    axios
      .get(API_URL)
      .then((response) => setProjectDatas(response.data.projects));
  }, []);
  return (
    <>
      {projectDatas.map((data, index) => {
        return <ProjectCard key={index} {...data} />;
      })}
    </>
  );
}
