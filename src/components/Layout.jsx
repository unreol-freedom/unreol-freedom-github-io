import React from "react";
import "../styles/navbar.css";
import "../styles/footer.css";
import { Link } from "react-router-dom";
import { Navbar, Container, Nav, NavDropdown } from "react-bootstrap";

export default function Layout({ children }) {
  return (
    <>
      <Navbar className="navbar" expand="md">
        <Container>
          <Navbar.Brand href="/" className="navbar-brand">
            Unreol Freedom
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Link className="react-router-link nav-link" to="/about">About</Link>

              <NavDropdown title="Contact" id="basic-nav-dropdown" active>
                <NavDropdown.Item>
                  1taylanozturk@protonmail.com
                </NavDropdown.Item>
                <NavDropdown.Item>ramazanemre@protonmail.com</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item>
                  unreolfreedom@protonmail.com
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <main>{children}</main>

      <footer className="site-footer">
        <div className="container">
          <div className="row">
            <div className="col-sm-12 col-md-6">
              <h6>About</h6>
              <p className="text-justify">
                Unreol Freedom is an organization that works for free software
                and is trying to create a free technology environment. It
                focuses on developing free softwares, projects that are useful,
                helpful for everyone and having a good community. It’s
                definitely not for profit. You can reach us via the contact
                links if you want to work with us. It has a friendly environment
                to chit-chat as well for teammates. It pays attention to fun,
                the one of the philosophies it has is learning and improving
                while having fun.
              </p>
            </div>

            <div className="col-xs-6 col-md-3">
              <h6>Company</h6>
              <ul className="footer-links">
                <li>
                  <span>
                    <Link to="/about">About Us</Link>
                  </span>
                </li>
                <li>
                  <span>
                    <Link to="/partnerships">Partnerships</Link>
                  </span>
                </li>
                <li>
                  <span>
                    <Link to="/developers">Developers</Link>
                  </span>
                </li>
              </ul>
            </div>

            <div className="col-xs-6 col-md-3">
              <h6>Quick Links</h6>
              <ul className="footer-links">
                <li>
                  <span>
                    <Link to="/contact">Contact</Link>
                  </span>
                </li>
                <li>
                  <a
                    href="https://github.com/Unreol-Freedom"
                    target="_blank"
                    rel="noreferrer"
                  >
                    Contribute
                  </a>
                </li>
                <li>
                  <a
                    href="https://unreol-freedom.github.io/blog"
                    target="_blank"
                    rel="noreferrer"
                  >
                    Blog
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <hr />
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-8 col-sm-6 col-xs-12">
              <p className="copyright-text">
                Ramazan Emre Osmanoğlu & Taylan Öztürk
              </p>
            </div>

            <div className="col-md-4 col-sm-6 col-xs-12">
              <ul className="social-icons">
                <li>
                  <a
                    className="github"
                    href="https://github.com/Unreol-Freedom"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <i className="fa fa-github"></i>
                  </a>
                </li>
                <li>
                  <a
                    className="twitter"
                    href="https://twitter.com/UnreolF"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <i className="fa fa-twitter"></i>
                  </a>
                </li>
                <li>
                  <a
                    className="linkedin"
                    href="https://www.linkedin.com/in/unreol-freedom-b758b3226/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <i className="fa fa-linkedin"></i>
                  </a>
                </li>
                <li>
                  <a
                    className="youtube"
                    href="https://www.youtube.com/channel/UC_Q3M4_AwhZZ30DUnPZ0CpQ"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <i className="fa fa-youtube"></i>
                  </a>
                </li>
                <li>
                  <a
                    className="telegram"
                    href="https://t.me/unreolfreedom"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <i className="fa fa-telegram"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}
