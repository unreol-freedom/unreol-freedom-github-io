import React, { Fragment } from "react";
import "../styles/projectcard.css";
import { Card, Button } from "react-bootstrap";
import ModifiedBadge from "./ModifiedBadge";

export default function ProjectCard(props) {
  let badgeArray = [];
  let badgeTags = [...props.tags];
  for (let i = 0; i < badgeTags.length; i++) {
    badgeArray.push(<ModifiedBadge tags={badgeTags[i]} />);
  }
  return (
    <Card className="card">
      <Card.Img
        variant="top"
        className="card--img"
        alt="image"
        src={props.image_url}
      ></Card.Img>
      <Card.Body className="card--body">
        <Card.Title>{props.title}</Card.Title>
        <Card.Subtitle className="mb-2">
          {badgeArray.map((element, index) => {
            return <Fragment key={index}>{badgeArray[index]}</Fragment>;
          })}
        </Card.Subtitle>
        <Card.Text className="mt-3">{props.description}</Card.Text>
        <a href={props.project_url} target="_blank" rel="noreferrer">
          <Button variant="success">Github</Button>
        </a>
      </Card.Body>
    </Card>
  );
}
