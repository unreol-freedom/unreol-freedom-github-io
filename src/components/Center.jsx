import React from "react";

export default function Center({ children }) {
  const style = {
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
  }
  return <div style={style}>{children}</div>;
}
