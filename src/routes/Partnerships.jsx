import React from "react";
import Center from "../components/Center";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Partnerships() {
  return (
    <Center>
      <div>
        <h1 className="text-white mt-5">Coming Soon</h1>
      </div>
      <div>
        <Button>
          <Link className="react-router-link" to="/">
            <span className="text-white"> Go Back </span>
          </Link>
        </Button>
      </div>
    </Center>
  );
}
