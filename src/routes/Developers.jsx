import React from "react";
import Center from "../components/Center";
import "../styles/developers.css";

export default function Developers() {
  return (
    <Center>
      <div className="developers">
        <div className="developer">
          <div className="developer--img">
            <img
              src="https://avatars.githubusercontent.com/u/81323808?v=4"
              alt="Avatar"
            />
          </div>
          <div className="developer--info">
            <a
              href="https://github.com/1TaylanOzturk"
              target="_blank"
              rel="noreferrer"
            >
              Taylan Öztürk
            </a>
            . He's a front-end developer. Ask he about Python, Javascript and
            React. Also he's interested in martial arts and playing chess. He
            also loves Manjaro.
          </div>
        </div>

        <div className="developer">
          <div className="developer--img">
            <img
              src="https://avatars.githubusercontent.com/u/66941061?v=4"
              alt="Avatar"
            />
          </div>
          <div className="developer--info">
            <a
              href="https://github.com/ramazanemreosmanoglu"
              target="_blank"
              rel="noreferrer"
            >
              R. Emre Osmanoğlu
            </a>
            . He's a programmer. Ask he about Python, Haskell, React Also he's a
            musician. He also loves Artix and Fedora.
          </div>
        </div>
      </div>
    </Center>
  );
}
