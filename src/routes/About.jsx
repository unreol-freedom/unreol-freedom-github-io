import React from "react";
import Center from "../components/Center";
import "../styles/about.css";

export default function About() {
  return (
    <Center>
      <div className="about">
        <div className="about--header">
          <h2>About Unreol Freedom</h2>
          <hr />
        </div>
        <div className="about--body">
          <div className="question-answer-section">
            <h4>What's Unreol Freedom ?</h4>
            <p>
              Unreol Freedom is an organization that works for free software and
              is trying to create a free technology environment. It focuses on
              developing free softwares, projects that are useful, helpful for
              everyone and having a good community. It’s definitely not for
              profit. You can reach us via the contact links if you want to work
              with us. It has a friendly environment to chit-chat as well for
              teammates. It pays attention to fun, the one of the philosophies
              it has is learning and improving while having fun.
            </p>
          </div>
          <div className="question-answer-section">
            <h4>Philosophy of UF: Free software</h4>
            <p>
              To us, free software and its awesome philosophy can change the
              world. Our goal is to create an environment that people prefer
              free software and people take care of softwares that they use are
              free. As Unreol Freedom, we are a non-profit organization.
              Everything is for a better environment, for a better internet.
              Proprietary software often limits us. It is not yours and will not
              be, it is the company’s even though you buy it. Because of the
              contract you approved while purchasing, you cannot see the source
              code of the software you purchased and be sure of its security,
              and you cannot share it with others. Limitations of proprietary
              software put on you don’t finish with only source codes of course.
              There's one more threat named DRM that is more problematic. But
              unlike proprietary software, free software is public and it
              belongs to everyone. Everyone that wants to benefit from your
              codes can read your codes, recommend something and even develop
              it. Your software is free for you and your users. There are four
              fundamentalic freedoms that software must provide in order to be
              free software.
            </p>
            <ol>
              <li>
                Freedom to use the software under any conditions for any purpose
                (Use)
              </li>
              <li>
                Freedom to understand how the software works and to change it.
                (Research)
              </li>
              <li>Freedom to duplicate and distribute the software. (Share)</li>
              <li>
                Freedom to distribute modified and enhanced copies of the
                software. (Develop)
              </li>
            </ol>
            Free softwares are which provide the all four freedoms above.
          </div>
        </div>
        <div className="about--footer"></div>
      </div>
    </Center>
  );
}
