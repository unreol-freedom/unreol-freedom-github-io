import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
import App from "./App";
import About from "./routes/About";
import Layout from "./components/Layout";
import Contact from "./routes/Contact";
import Developers from "./routes/Developers";
import Partnerships from "./routes/Partnerships";

ReactDOM.render(
  <BrowserRouter>
    <Routes>
      <Route
        path="/"
        element={
          <Layout>
            <App />
          </Layout>
        }
      />
      <Route
        path="/about"
        element={
          <Layout>
            <About />
          </Layout>
        }
      />
      <Route
        path="/contact"
        element={
          <Layout>
            <Contact />
          </Layout>
        }
      />
      <Route
        path="/developers"
        element={
          <Layout>
            <Developers />
          </Layout>
        }
      />

      <Route path="/partnerships" element={<Partnerships />} />
    </Routes>
  </BrowserRouter>,
  document.getElementById("root")
);
